﻿using UnityEngine;
using System.Collections;
using CompleteProject;

public class EnemyMovement : MonoBehaviour
{
    Transform player;
    Done_PlayerHealth playerHealth;
    Done_EnemyHealth enemyHealth;
    UnityEngine.AI.NavMeshAgent nav;


    void Awake ()
    {
        player = GameObject.FindGameObjectWithTag ("Player").transform;
        playerHealth = player.GetComponent <Done_PlayerHealth> ();
        enemyHealth = GetComponent <Done_EnemyHealth> ();
        nav = GetComponent <UnityEngine.AI.NavMeshAgent> ();
    }


    void Update ()
    {
        if(enemyHealth.currentHealth > 0 && playerHealth.currentHealth > 0)
        {
            nav.SetDestination (player.position);
        }
        else
        {
            nav.enabled = false;
        }
    }
}
