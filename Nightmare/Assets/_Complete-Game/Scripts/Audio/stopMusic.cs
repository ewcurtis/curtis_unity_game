﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CompleteProject
{
    //This will stop the Background music when the Player wins the game
    public class stopMusic : MonoBehaviour
    {
        AudioSource music;
        //AudioClip bMusic;


        // Start is called before the first frame update
        void Start()
        {
            music = GetComponent<AudioSource>();
            

            
        }

        // Update is called once per frame
        void Update()
        {
            if (WinOrLose.win)
            {
                music.Stop();

            }
        }
    }
}