﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace CompleteProject
{
public class WinOrLose : MonoBehaviour
{

    public static bool win = false;
    Text text;
    int count;
    Done_PlayerHealth playerHealth;
    GameObject player;

    // Start is called before the first frame update
    void Start()
    {

        text = GetComponent<Text>();

        // Setting up the references.
        player = GameObject.FindGameObjectWithTag("Player");
        playerHealth = player.GetComponent<Done_PlayerHealth>();
    }

    // Update is called once per frame
    void Update()
    {

        if (win && count == 0)
        {
            text.text = "You Win!";
            //Kills player to end game
            playerHealth.currentHealth = 0;
            count += 1;
            //Allows the game to reset properly
            win = false;
        }
        
    }
}
}