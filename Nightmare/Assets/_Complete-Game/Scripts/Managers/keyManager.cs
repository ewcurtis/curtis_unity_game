﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class keyManager : MonoBehaviour
{
    public static string total = "/3";

    public static int keys = 0;

    Text text;

    void Awake()
    {  

        text = GetComponent <Text> ();

        keys = 0;
        
    }

    // Update is called once per frame
    void Update()
    {
        text.text = "Keys: " + keys + total;       
   
    }
}
