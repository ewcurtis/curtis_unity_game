﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class keySpawnManager : MonoBehaviour
{

    public GameObject key;
    public Transform[] spawnPoints;
    // Start is called before the first frame update
    void Start()
    {

        Spawn();
       
    }

    void Spawn()
    {

        // Find a random index between zero and one less than the number of spawn points.
        int spawnPointIndex = Random.Range(0, spawnPoints.Length);

        // Create an instance of the enemy prefab at the randomly selected spawn point's position and rotation.
        Instantiate(key, spawnPoints[spawnPointIndex].position, spawnPoints[spawnPointIndex].rotation);

    }
    

    
}
